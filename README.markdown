# Video platform using plyr and luxa

A Pen created on CodePen.io. Original URL: [https://codepen.io/luxonauta/pen/ExKbaOO](https://codepen.io/luxonauta/pen/ExKbaOO).

A simple page, created with luxa and using plyr. With skip options for a particular chapter and descriptions.

[DEMO](https://readloud.github.io/video-platform-using-plyr-and-luxa/dist)
